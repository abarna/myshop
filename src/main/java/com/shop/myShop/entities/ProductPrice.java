package com.shop.myShop.entities;

import com.shop.myShop.enums.Currency;
import lombok.Data;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
public class ProductPrice {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    private Date from_date;
    private Date to_date;
    private double price;
    @Enumerated(STRING)
    private Currency currency;
}
