package com.shop.myShop.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;
    private String name;
    @Column(unique = true)
    private String code;
    @OneToMany(mappedBy = "product", cascade = ALL)
    private List<ProductPrice> prices;
    private boolean valid;
}
