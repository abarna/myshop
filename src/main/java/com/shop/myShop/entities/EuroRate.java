package com.shop.myShop.entities;

import com.shop.myShop.enums.Currency;
import lombok.Data;

import javax.persistence.*;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
public class EuroRate {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;
    @Enumerated(STRING)
    @Column(unique = true)
    private Currency currency;
    private double rate;
}
