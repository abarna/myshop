package com.shop.myShop.configuration;

import com.shop.myShop.batch.priceBatchExporter.processor.ProductProcessor;
import com.shop.myShop.batch.priceBatchExporter.reader.ProductReader;
import com.shop.myShop.batch.priceBatchExporter.writer.ProductWriter;
import com.shop.myShop.dtos.ProductDto;
import com.shop.myShop.entities.Product;
import com.shop.myShop.exceptions.InvalidProductException;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
public class BatchConfiguration {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    @Bean
    @StepScope
    public ProductReader productReader() {
        return new ProductReader();
    }

    @Bean
    public ProductProcessor productProcessor() {
        return new ProductProcessor();
    }

    @Bean
    @StepScope
    public ProductWriter productWriter() {
        return new ProductWriter();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<ProductDto, ProductDto>chunk(2)
                .reader(productReader())
                .processor(productProcessor())
                .writer(productWriter())
                .faultTolerant()
                .skip(InvalidProductException.class)
                .build();
    }

    @Bean
    public Job exportPricesJob() {
        return jobBuilderFactory.get("exportPricesJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }
}
