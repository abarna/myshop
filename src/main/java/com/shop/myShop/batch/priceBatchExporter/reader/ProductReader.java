package com.shop.myShop.batch.priceBatchExporter.reader;

import com.shop.myShop.dtos.ProductDto;
import com.shop.myShop.repositories.ProductRepository;
import com.shop.myShop.services.product.ProductService;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ProductReader implements ItemReader<ProductDto> {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductService productService;
    private Iterator<ProductDto> products;

    @PostConstruct
    public void loadProducts() {
        products = StreamSupport.stream(productRepository.findAll().spliterator(), true)
                .map(product -> {
                    ProductDto productPrice = null;
                    try {
                        productPrice = productService.getProductPrice(product.getCode());
                    } catch (Exception e) {
                        // here the exception should be handled
                        e.printStackTrace();
                    }
                    return productPrice;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList())
                .iterator();
    }

    @Override
    public ProductDto read() {
        if (products.hasNext()) {
            return products.next();
        }
        return null;
    }
}
