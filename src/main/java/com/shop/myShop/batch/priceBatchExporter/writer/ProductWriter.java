package com.shop.myShop.batch.priceBatchExporter.writer;

import com.shop.myShop.dtos.ProductDto;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.batch.item.ItemWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class ProductWriter implements ItemWriter<ProductDto> {
    private int chunkNumber;

    @Override
    public void write(List<? extends ProductDto> productsDto) throws IOException {
        chunkNumber++;
        FileWriter out = new FileWriter("productPrices_" + chunkNumber + ".csv");
        String[] HEADERS = {"code", "price", "currency"};
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT
                .withHeader(HEADERS))) {

            createCSVFile(productsDto, printer);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createCSVFile(List<? extends ProductDto> productsDto, CSVPrinter printer) {
        productsDto.forEach(productDto -> {

            printToConsole(productDto);

            try {
                printer.printRecord(productDto.getCode(), productDto.getPrice(), productDto.getCurrency());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void printToConsole(ProductDto productDto) {
        System.out.println("################# Adding in CSV #########");
        System.out.println(productDto);
        System.out.println("#########################################");
    }
}
