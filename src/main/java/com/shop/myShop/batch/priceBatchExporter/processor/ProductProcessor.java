package com.shop.myShop.batch.priceBatchExporter.processor;

import com.shop.myShop.dtos.ProductDto;
import org.springframework.batch.item.ItemProcessor;

public class ProductProcessor implements ItemProcessor<ProductDto, ProductDto> {
    @Override
    public ProductDto process(ProductDto product) {
        // this is not neccessary, is just for demo purposes
        return product;
    }
}
