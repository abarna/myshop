package com.shop.myShop.services.rate.impl;

import com.shop.myShop.entities.EuroRate;
import com.shop.myShop.enums.Currency;
import com.shop.myShop.repositories.EuroRateRepository;
import com.shop.myShop.services.rate.RateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RateServiceImpl implements RateService {
    private final EuroRateRepository euroRateRepository;


    @Override
    public Double getPriceInEur(Double notInEurPrice, Currency fromCurency) {
        EuroRate euroRate = euroRateRepository.findFirstByCurrency(fromCurency);
        return notInEurPrice * euroRate.getRate();
    }
}
