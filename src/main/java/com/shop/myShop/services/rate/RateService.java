package com.shop.myShop.services.rate;

import com.shop.myShop.enums.Currency;

public interface RateService {
    Double getPriceInEur(Double value, Currency from);
}
