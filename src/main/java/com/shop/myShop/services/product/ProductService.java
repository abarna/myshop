package com.shop.myShop.services.product;

import com.shop.myShop.dtos.ProductDto;
import com.shop.myShop.exceptions.InvalidCodeException;
import com.shop.myShop.exceptions.InvalidPriceException;
import com.shop.myShop.exceptions.InvalidProductException;

public interface ProductService {
    ProductDto getProductPrice(String productCode) throws InvalidCodeException, InvalidProductException, InvalidPriceException;
}
