package com.shop.myShop.services.product.impl;

import com.shop.myShop.dtos.ProductDto;
import com.shop.myShop.entities.Product;
import com.shop.myShop.entities.ProductPrice;
import com.shop.myShop.exceptions.InvalidCodeException;
import com.shop.myShop.exceptions.InvalidPriceException;
import com.shop.myShop.exceptions.InvalidProductException;
import com.shop.myShop.repositories.ProductRepository;
import com.shop.myShop.services.product.ProductService;
import com.shop.myShop.services.rate.RateService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

import static com.shop.myShop.enums.Currency.EUR;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final RateService rateService;

    @Override
    @Transactional
    public ProductDto getProductPrice(String productCode) throws InvalidCodeException, InvalidProductException, InvalidPriceException {
        Optional<Product> productOptional = productRepository.findByCode(productCode);

        validateProduct(productOptional);

        val product = productOptional.get();
        val currentProductPrice = getCurrentProductPrice(product);

        if (hasCurrencyInEUR(currentProductPrice)) {
            return new ProductDto(productCode, currentProductPrice.getPrice(), EUR);
        }
        Double priceInEUR = rateService.getPriceInEur(currentProductPrice.getPrice(),
                currentProductPrice.getCurrency());
        return new ProductDto(productCode, priceInEUR, EUR);
    }

    private ProductPrice getCurrentProductPrice(Product product) throws InvalidPriceException {
        return product.getPrices()
                .stream()
                .filter(productPrice -> {
                    val currentDate = new Date();
                    return productPrice.getFrom_date().before(currentDate) &&
                            productPrice.getTo_date().after(currentDate);
                })
                .findFirst()
                .orElseThrow(InvalidPriceException::new);
    }

    private boolean hasCurrencyInEUR(ProductPrice productPrice) {
        return EUR.equals(productPrice.getCurrency());
    }

    private void validateProduct(Optional<Product> productOptional) throws InvalidCodeException, InvalidProductException {
        val product = productOptional.orElseThrow(InvalidCodeException::new);
        if (!product.isValid()) {
            throw new InvalidProductException();
        }
    }
}
