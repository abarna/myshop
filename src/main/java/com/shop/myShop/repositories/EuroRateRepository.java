package com.shop.myShop.repositories;

import com.shop.myShop.entities.EuroRate;
import com.shop.myShop.enums.Currency;
import org.springframework.data.repository.CrudRepository;

public interface EuroRateRepository extends CrudRepository<EuroRate, Long> {
    EuroRate findFirstByCurrency(Currency currency);
}
