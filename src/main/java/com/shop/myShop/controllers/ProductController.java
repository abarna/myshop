package com.shop.myShop.controllers;

import com.shop.myShop.dtos.ProductDto;
import com.shop.myShop.dtos.ProductResponseDto;
import com.shop.myShop.enums.Currency;
import com.shop.myShop.exceptions.InvalidCodeException;
import com.shop.myShop.exceptions.InvalidPriceException;
import com.shop.myShop.exceptions.InvalidProductException;
import com.shop.myShop.services.product.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;


    @GetMapping("/{productCode}")
    public ResponseEntity<ProductResponseDto> getPrice(@PathVariable String productCode) {
        ProductDto productDto;
        try {
            productDto = productService.getProductPrice(productCode);
        } catch (InvalidCodeException e) {
            return status(BAD_REQUEST).body(new ProductResponseDto("The code is invalid"));
        } catch (InvalidProductException e) {
            return status(BAD_REQUEST).body(new ProductResponseDto("The product is invalid"));
        } catch (InvalidPriceException e) {
            return status(BAD_REQUEST).body(new ProductResponseDto("The product has an invalid price"));
        }
        return status(OK).body(new ProductResponseDto(productDto.getPrice(), productDto.getCurrency()));
    }
}
