package com.shop.myShop.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.shop.myShop.enums.Currency;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@JsonInclude(NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    protected String code;
    protected Double price;
    protected Currency currency;
}
