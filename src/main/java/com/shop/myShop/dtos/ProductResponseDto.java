package com.shop.myShop.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.shop.myShop.enums.Currency;
import lombok.Getter;
import lombok.Setter;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Setter
@Getter
@JsonInclude(NON_NULL)
public class ProductResponseDto extends ProductDto {
    private String error;

    public ProductResponseDto(Double price, Currency currency) {
        this.price = price;
        this.currency = currency;
    }

    public ProductResponseDto(String error) {
        this.error = error;
    }
}
