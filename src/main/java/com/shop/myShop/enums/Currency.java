package com.shop.myShop.enums;

public enum Currency {
    EUR,
    USD,
    RON
}
