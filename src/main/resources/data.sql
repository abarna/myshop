insert into euro_rate (currency, rate)
values ('USD', 0.9),
       ('RON', 0.7);
insert into product(name, code, valid)
values ('Product in USD', 'usdCode', true),
       ('Product in RON', 'ronCode', true),
       ('Product in EUR', 'eurCode', true),
       ('Product in USD', 'invalidRonCode', false);

-- INSERTING PRICES FOR FIRST PRODUCT
INSERT INTO product_price( currency, price, from_date, to_date,product_id)
values( 'USD', 100, now() - 1, now(), 1);
INSERT INTO product_price( currency, price, from_date, to_date,product_id)
values( 'USD', 200, now(), now() +1, 1);

-- INSERTING PRICES FOR SECOND PRODUCT
INSERT INTO product_price( currency, price, from_date, to_date,product_id)
values( 'RON', 50, now() - 1, now(), 2);
INSERT INTO product_price( currency, price, from_date, to_date,product_id)
values( 'RON', 70, now(), now() +1, 2);

-- INSERTING PRICES FOR THIRD PRODUCT
INSERT INTO product_price( currency, price, from_date, to_date,product_id)
values( 'EUR', 50, now() - 1, now(), 3);
INSERT INTO product_price( currency, price, from_date, to_date,product_id)
values( 'EUR', 70, now(), now() +1, 3);
