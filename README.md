Project which exposes an endpoint for retriving the dynamic price of a product.

Has as dependencies:
- Spring Boot
- Spring Data JPA
- Spring Batch
- Spring Quartz 
- H2 database
- Lombok
